# TuringRun

TuringRun is a simple Turing machine simulator.

![](screenshot.png)

Instructions:

```S(x)``` Increment `x`

```Z(x)``` Set `x` to `0`

```T(x,y)``` Copy `x` to `y`

```I(x,y,z)``` If `x` == `y` jump to `z`

Sample program:

    S(1)
    S(1)
    S(1)

    S(0)
    I(0,1,7)
    I(0,0,4)