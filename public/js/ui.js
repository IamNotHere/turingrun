const memoryElem = document.getElementById("memoryContainer");
const INS_COUNTElem = document.getElementById("ins_count");
const STATEElem = document.getElementById("prog_state");
const buttonElems = document.getElementById("controlButtons").querySelectorAll("button");

const demoCell = document.createElement("div"); {
    let valElem = document.createElement("div");
    valElem.innerHTML = "VAL";
    valElem.classList.add("value")

    let idElem = document.createElement("div");
    idElem.innerHTML = "#ID";
    idElem.classList.add("id");

    demoCell.appendChild(valElem);
    demoCell.appendChild(idElem);
    demoCell.classList.add("memoryCell");
    demoCell.classList.add("demoCell");
}

class UIClass {
    parentElem = {};
    startBtn = {};
    stopBtn = {};
    stepBtn = {};
    resetBtn = {};
    editorTextArea = {};

    constructor(parentElem) {
        this.parentElem = parentElem;
        for (let i = 0; i < buttonElems.length; i++) {
            const curr = buttonElems[i];
            
            switch (curr.value) {
                case "exec":
                    this.startBtn = curr;
                    break;
                case "stop":
                    this.stopBtn = curr;
                    break;
                case "step":
                    this.stepBtn = curr;
                    break;
                case "reset":
                    this.resetBtn = curr;
                    break;

                default:
                    break;
            }
        }
    }

    update(state) {
        memoryElem.innerHTML = "";
        memoryElem.appendChild(demoCell)
        this.appendMemoryCellElems(state);
        this.setButtonStates(state);
        this.setDescStates(state);

        this.setInputStatus(!state.STATE);
    }

    _setEditorSrc(src) {
        this.editorTextArea = src;
    }

    setInputStatus(enabled) {
        if (enabled) {
            this.editorTextArea.disabled = false;
        } else {
            this.editorTextArea.disabled = true;
        }
    }

    appendMemoryCellElems(state) {
        this.parentElem.innerHTML = "";
        this.parentElem.appendChild(demoCell);
        for (let i = 0; i < state.MEM.length; i++) {
            const curr = state.MEM[i];

            let cell = document.createElement("div"); {
                let valElem = document.createElement("div");
                valElem.innerHTML = curr;
                valElem.classList.add("value")
            
                let idElem = document.createElement("div");
                idElem.innerHTML = "#" + i;
                idElem.classList.add("id");
            
                cell.appendChild(valElem);
                cell.appendChild(idElem);
                cell.classList.add("memoryCell");
            }

            this.parentElem.appendChild(cell);
        }
    }

    setDescStates(state) {
        INS_COUNTElem.innerHTML = state.INS_COUNT+1;

        if (state.STATE) {
            // Program running
            STATEElem.innerHTML = "RUN";
            STATEElem.style.color = "var(--clr-green)";
        } else {
            // Program halt
            STATEElem.innerHTML = "HALT";
            STATEElem.style.color = "var(--clr-red)";
        }
    }

    setButtonStates(state) {
        if (state.STATE) {
            // Program running
            this.startBtn.disabled = true;
            this.stepBtn.disabled = true;
            this.stopBtn.disabled = false;
        } else {
            // Program halt
            this.startBtn.disabled = false;
            this.stepBtn.disabled = false;
            this.stopBtn.disabled = true;
        }
    }
}