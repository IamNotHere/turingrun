const textarea = document.getElementById("editorSrc");
const lineNums = document.getElementById("lineNums");

const globalState = new State(triggerEditorEvents);
globalState.speed = 100;

const UI = new UIClass(memoryElem);
UI._setEditorSrc(textarea)

textarea.addEventListener("input", (e) => {editorEvent(e, globalState)});
textarea.addEventListener("keydown", (e) => {editorEvent(e, globalState)});
textarea.addEventListener("keyup", (e) => {editorEvent(e, globalState)});

function editorEvent(e, state) {
    setLineNumbers(e, state);
    globalState.ops = parseOps(globalState);
    globalState.memAutoSize();

    // UI
    UI.update(globalState);
}

function triggerEditorEvents() {
    textarea.dispatchEvent(new Event("input"));
}
triggerEditorEvents();

// Avoid edge case lockout
setInterval(() => {
    triggerEditorEvents();
}, 1000);

// Bind buttons
UI.startBtn.addEventListener("click", (e) => {
    globalState.start();
    triggerEditorEvents();
})
UI.stopBtn.addEventListener("click", (e) => {
    globalState.stop();
    triggerEditorEvents();
})
UI.stepBtn.addEventListener("click", (e) => {
    globalState.execStep();
    triggerEditorEvents();
})
UI.resetBtn.addEventListener("click", (e) => {
    globalState.reset();
    triggerEditorEvents();
})