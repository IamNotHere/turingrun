Array.prototype.max = function() {
    return Math.max.apply(null, this);
};

Array.prototype.min = function() {
    return Math.min.apply(null, this);
};

class State {
    // Line array
    lineArr = [];
    // Line Mods
    lineMod = {}
    // Operators array
    ops = [];
    // Memory cells
    MEM = [];
    // IC
    INS_COUNT = 0;
    // Program state
    STATE = true
    // Program speed
    speed = 1
    // Callback for updates
    callback = ()=>{};
    constructor(callback) {
        this.callback = callback;

        this.lineArr = [];
        this.ops = [];
        this.MEM = [];
        this.INS_COUNT = 0;
        this.lineMod = new LineMod;

        this.STATE = false;

        this.speed = 100;
    }

    _debugGetOps() {
        for (let i = 0; i < this.ops.length; i++) {
            const curr = this.ops[i];
            console.log(curr.type, curr.line, curr.realLine, curr.argv);
        }
        console.log(this.ops.length);
    }

    getSpeed() {
        return this.speed;
    }

    getState() {
        return this.STATE;
    }

    memSetZero(x) {
        if (x < 1) {
            x = 1;
        }

        this.MEM = [];

        for (let i = 0; i < x; i++) {
            this.MEM[i] = 0;
        }
    }

    memAutoSize() {
        let maxAddr = 0;
        for (let i = 0; i < this.ops.length; i++) {
            const curr = this.ops[i];
            let size = curr.getHighestMemAddr();
            if (size > maxAddr) {
                maxAddr = size;
            }
        }
        
        while (this.MEM.length > maxAddr+1) {
            this.MEM.pop();
        }
        while (this.MEM.length <= maxAddr+1) {
            this.MEM.push(0);
        }
    }

    step() {
        if (!this.STATE) {
            return false;
        }
        this.INS_COUNT = this.INS_COUNT*1 + 1;

        if (this.INS_COUNT > this.ops.length || !Number.isInteger(this.INS_COUNT)) {
            this.STATE = false;
            this.INS_COUNT = 0;
            this.callback();
            return false;
        }
        let tempState = this.ops[this.INS_COUNT-1].eval(this);
        this.MEM = tempState.MEM;
        this.callback();
        this.INS_COUNT = tempState.INS_COUNT;
    }

    run() {
        let speedfn = this.getSpeed.bind(this);
        let statefn = this.getState.bind(this);
        let stepfn = this.step.bind(this);
        function loop() {
            stepfn();
            if (statefn()) {
                setTimeout(() => {
                    loop();
                }, speedfn());
            }
        }
        loop();
    }

    execStep() {
        this.STATE = true;
        this.step();
        this.STATE = false;
    }

    start() {
        this.STATE = true;
        this.INS_COUNT = 0;
        this.memSetZero(this.MEM.length);
        this.run();
    }

    stop() {
        this.STATE = false;
    }

    reset() {
        this.INS_COUNT = 0;
        this.memSetZero(this.MEM.length);
    }
}

class LineMod {
    // Line highliters
    lineWarn = [];
    lineErr = [];
    constructor() {
        this.lineErr = [];
        this.lineWarn = [];
    }

    clearAll() {
        this.lineErr = [];
        this.lineWarn = [];
    }

    addWarn(line) {
        this.lineWarn.push(line);
    }

    addErr(line) {
        this.lineErr.push(line);
    }

    testWarn(index) {
        return this.lineWarn.includes(index);
    }

    testErr(index) {
        return this.lineErr.includes(index);
    }
}

class Operation {
    type = "";
    argv = [];
    valid = true;
    line = 0;
    realLine = 0;
    constructor(type, args, line, realLine) {
        this.realLine = realLine;
        this.line = line;
        this.type = type;
        if (!Array.isArray(args)) {
            this.valid = false;
            return false;
        }

        // this.argv = args;
        for (let i = 0; i < args.length; i++) {
            const curr = args[i];
            this.argv[i] = curr[0]*1;
        }

        for (let i = 0; i < this.argv.length; i++) {
            const curr = this.argv[i];
            if (curr < 0) {
                this.valid = false;
                return false;
            }
        }

        switch (type) {
            case "S":
                if (this.argv.length != 1) {
                    this.valid = false;
                    return false;
                }
                break;
            case "I":
                if (this.argv.length != 3) {
                    this.valid = false;
                    return false;
                }
                break;
            case "Z":
                if (this.argv.length != 1) {
                    this.valid = false;
                    return false;
                }
                break;
            case "T":
                if (this.argv.length != 2) {
                    this.valid = false;
                    return false;
                }
                break;
        
            default:
                this.valid = false;
                return false;
        }
    }

    eval(state) {
        if (!this.valid) {
            return false;
        }
        switch (this.type) {
            case "S":
                state.MEM[this.argv[0]] = state.MEM[this.argv[0]]+1;
                break;
            case "Z":
                state.MEM[this.argv[0]] = 0;
                break;
            case "T":
                state.MEM[this.argv[1]] = state.MEM[this.argv[0]];
                break;
            case "I":
                if (state.MEM[this.argv[1]] == state.MEM[this.argv[0]]) {
                    state.INS_COUNT = (this.argv[2]*1) - 1;
                }
                break;
            
            default:
                break;
        }
        return {
            MEM : state.MEM,
            INS_COUNT : state.INS_COUNT
        };
    }

    getArgv() {
        return this.argv;
    }

    getHighestMemAddr() {
        if (!this.valid) {
            return 0;
        }
        let tmp = [];
        tmp[0] = this.argv[0];
        tmp[1] = this.argv[1] ? this.argv[1] : 0;
        
        return tmp.max();
    }
}

function setLineNumbers(e, state) {
    var text = e.target.value;
    var lines = text.split(/\r|\r\n|\n/);

    state.ops = parseOps(state);

    state.lineArr = [];

    for (let i = 0; i < lines.length; i++) {
        state.lineArr[i] = {};
        state.lineArr[i].content = lines[i];
        state.lineArr[i].key = 0;
        state.lineArr[i].realLineNum = i+1;
    }

    e.target.value = text.replace( /(\n[a-z]|^[a-z])/, x => x.toUpperCase());

    function setLineNums(e) {
        if (lines.length < 10) {
            e.target.rows = 10;
        } else {
            e.target.rows = lines.length;
        }
        
        lineNums.innerHTML = "";
        let linesTotal = 0;
        for (let i = 0; i < lines.length; i++) {
            let elem = document.createElement("div");
            if (state.lineMod.testWarn(i)) {
                elem.classList.add("warn");
            }
            if (state.lineMod.testErr(i)) {
                elem.classList.add("err");
            }
            if (/[A-Z]{1}/.test(lines[i][0])) {
                linesTotal++;
                elem.innerHTML = linesTotal;
                state.lineArr[i].key = linesTotal;
                if (linesTotal == state.INS_COUNT+1) {
                    elem.classList.add("active");
                }
            } else if (/\//.test(lines[i][0])) {
                elem.innerHTML = "/";
                state.lineArr[i].key = -1;
            } else {
                elem.innerHTML = "|";
                state.lineArr[i].key = -1;
            }
            
            lineNums.appendChild(elem);
        }
    }
    setLineNums(e)
}

function parseOps(state) {
    let lineArr = [];
    // Filter out non-code
    for (let i = 0; i < state.lineArr.length; i++) {
        const curr = state.lineArr[i];
        if (curr.key != -1) {
            lineArr.push(curr);
        }
    }
    
    let operations = [];
    for (let i = 0; i < lineArr.length; i++) {
        const curr = lineArr[i];

        const validateRegex = /([0-9]+)/g
        const filterRegex = /[A-Z]\(([0-9]+,{0,1})*([0-9]+)+\)( \/\/.*)?$/
        let args;
        if (curr.content.match(filterRegex)) {
            args = Array.from(curr.content.matchAll(validateRegex));
        } else {
            args = 0;
        }
        
        operations.push(new Operation(curr.content[0], args, curr.key, curr.realLineNum, state.MEM));
    }
    lineArr = [];
    
    // Highlight errored lines
    state.lineMod.lineErr = [];
    for (let i = 0; i < operations.length; i++) {
        const curr = operations[i];
        if (curr.valid == false) {
            state.lineMod.addErr(curr.realLine-1);
        }
    }
    return operations;
}